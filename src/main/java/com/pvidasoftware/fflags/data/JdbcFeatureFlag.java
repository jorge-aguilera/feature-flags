package com.pvidasoftware.fflags.data;

import com.pvidasoftware.fflags.aop.FeatureFlagService;
import jakarta.inject.Singleton;

import java.util.concurrent.atomic.AtomicInteger;

@Singleton
public class JdbcFeatureFlag implements FeatureFlagService {

    AtomicInteger test1 = new AtomicInteger();
    AtomicInteger test2 = new AtomicInteger();

    public boolean check(String feature){

        switch( feature ){
            case "test1":
                return test1.incrementAndGet() % 2 == 0;
            case "test2":
                return test2.incrementAndGet() % 2 == 0;
            default:
                return false;
        }

    }

}
