package com.pvidasoftware.fflags.data;

public enum FeatureFlagsValues {

    Null(null),

    EmptyString(""),

    Zero(0)
    ;

    public final Object supplier;

    FeatureFlagsValues(Object supplier) {
        this.supplier = supplier;
    }

}
