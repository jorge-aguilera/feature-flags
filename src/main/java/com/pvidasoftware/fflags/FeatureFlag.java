package com.pvidasoftware.fflags;

import com.pvidasoftware.fflags.aop.FeatureFlagInterceptor;
import com.pvidasoftware.fflags.data.FeatureFlagsValues;
import io.micronaut.aop.Around;
import io.micronaut.context.annotation.Type;

import java.lang.annotation.*;

@Documented
@Target(ElementType.METHOD)
@Retention(RetentionPolicy.RUNTIME)
@Around
@Type(FeatureFlagInterceptor.class)
public @interface FeatureFlag {

    String flag();

    boolean throwRuntimeException() default false;

    FeatureFlagsValues defaultValue() default FeatureFlagsValues.Null;
}
