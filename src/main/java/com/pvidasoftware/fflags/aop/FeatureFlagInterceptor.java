package com.pvidasoftware.fflags.aop;

import com.pvidasoftware.fflags.FeatureFlag;
import com.pvidasoftware.fflags.data.FeatureFlagsValues;
import io.micronaut.aop.InterceptorBean;
import io.micronaut.aop.MethodInterceptor;
import io.micronaut.aop.MethodInvocationContext;
import jakarta.inject.Singleton;

import java.util.Optional;

@InterceptorBean(FeatureFlag.class)
@Singleton
public class FeatureFlagInterceptor implements MethodInterceptor<Object, Object> {

    FeatureFlagService featureFlagService;

    public FeatureFlagInterceptor(FeatureFlagService featureFlagService) {
        this.featureFlagService = featureFlagService;
    }

    @Override
    public Object intercept(MethodInvocationContext<Object, Object> context) {
        String fflag = context.stringValue(FeatureFlag.class, "flag").get();
        Optional<Boolean> exception = context.booleanValue(FeatureFlag.class, "throwRuntimeException");
        Optional<FeatureFlagsValues> defaultValue = context.getValue(FeatureFlag.class, "defaultValue", FeatureFlagsValues.class);

        if( !featureFlagService.check(fflag) ){
            if( exception.isPresent() && exception.get()){
                throw new RuntimeException(String.format("Flag %s present", fflag));
            }
            if(defaultValue.isPresent()){
                return defaultValue.get().supplier;
            }
            return null;
        }

        return context.proceed();
    }
}
