package com.pvidasoftware.fflags.aop;

public interface FeatureFlagService {

    public boolean check(String feature);

}
