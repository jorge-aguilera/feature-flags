package com.pvidasoftware;

import com.pvidasoftware.fflags.FeatureFlag;
import com.pvidasoftware.fflags.data.FeatureFlagsValues;
import io.micronaut.http.annotation.Controller;
import io.micronaut.http.annotation.Get;

@Controller
public class HelloController {

    @Get("/hi")
    String indexFeature1(){
        return feature1()+" hi man";
    }

    @Get("/hola")
    String indexFeature2(){
        return feature2()+" hi man";
    }

    @FeatureFlag(flag = "test1", defaultValue = FeatureFlagsValues.EmptyString)
    protected String feature1(){
        return "Welcome";
    }

    @FeatureFlag(flag = "test2", throwRuntimeException = true)
    protected String feature2(){
        int a = 10;
        a++;
        if( a > 1)
            return "Mr";
        else
            return "Ms";
    }

}
