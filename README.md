## Micronaut Feature Flags (PoC)

This is a Proof of concept (Poc) about AOP in Micronaut

The idea is to create a `FeatureFlag` annotation for methods who will be executed only if the specified 
`feature` is active,

Suppose we have a new feature to return "Welcome" when the method it's called, but we want to no return nothing
until all production checks are passed. Our method will be something as:

```
@FeatureFlag(flag = "product1.controller.welcome", defaultValue = FeatureFlagsValues.EmptyString)
protected String header(){
    // new business logic
    String ret = "W";
    ret += "elcome;
    return ret;
}
```

In case `product1.controller.welcome` flag is active (can be a `true` in a database, and env value, ...) the method
will be executed and "Welcome" will be returned. In case is not active a default value will be return (or null if
not specified)

## Annotation

FeatureFlag.java:

```
@Documented
@Target(ElementType.METHOD)
@Retention(RetentionPolicy.RUNTIME)
@Around
@Type(FeatureFlagInterceptor.class)
public @interface FeatureFlag {

    String flag();

    boolean throwRuntimeException() default false;

    FeatureFlagsValues defaultValue() default FeatureFlagsValues.Null;
} 
```

